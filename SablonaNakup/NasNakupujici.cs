﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*namespace SablonaNakup
{
    class NasNakupujici : Nakupujici
    {
        private int pocet_kol;
        private char[,] mapa = new char[80, 20];
        Smer pomocna = new Smer { X = 0, Y = 0 };
        private Smer[] PredchoziTahy = new Smer[3];
        public NasNakupujici()
        {
            pocet_kol = 1;
            Nabidky[] nabidky = new Nabidky[25];

        }
        //  NovaNabidka ziska jakou hodnotu ma  nabidka
        // a na jakych je souradnicich i, j
        // vytvori novou nabidku a vrati ji
        Nabidky NovaNabidka(int nabidka, int i, int j)
        {
            Nabidky nova;
            nova.kolo = pocet_kol;
            nova.cena = nabidka;
            nova.x = i;
            nova.y = j;
            return nova;
        }
        //Nabidka dostane cast mapy, pole na pripadne nabidky a kolik policek je zaplnenych (0)
        //zjisti zda mapa obsahuje nabidky a pripadne je zapise do pole nabidek
        // vraci true pokud nasla nabidku
        public bool Nabidka(char[,] castMapy, Nabidky[] seznamNabidek, int volno, Zlodejik[] seznamZlodeju, int zaplneniZ)
        {
            bool nabidka = false;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (castMapy[i, j] > 0 && castMapy[i, j] < 10)
                    {
                        seznamNabidek[volno] = NovaNabidka(castMapy[i, j], i, j);
                        volno++;
                        nabidka = true;
                    }
                    else
                    {
                        if (nabidka != true)
                        {
                            nabidka = false;
                        }
                    }
                    if (castMapy[i, j] >= 'A' && castMapy[i, j] < 'X')
                    {
                        // DOPSAT POKUD JE ZLODEJ TAK AT SE TO NASTAVI NA TRUE
                        seznamZlodeju[zaplneniZ].x = i;
                        seznamZlodeju[zaplneniZ].y = j;
                        zaplneniZ++;
                    }
                }
            }
            return nabidka;
        }
        Smer krokKNAbidce(char[,] castMapy, Nabidky nova)
        {
            Smer odpoved = new Smer();
            if ((nova.x == 1 && ((nova.y == 1) || (nova.y == 2) || (nova.y == 3))) || (nova.x == 3 && ((nova.y == 1) || (nova.y == 2) || (nova.y == 3))) || (nova.x == 2 && ((nova.y == 1) || (nova.y == 3))))
            {
                odpoved.X = (sbyte)(nova.x - 2);
                odpoved.Y = (sbyte)(nova.y - 2);
            }
            else
            {
                if (nova.x > 2)
                {
                    odpoved.X = (sbyte)(nova.x - 1 - 2);
                }
                else if (nova.x <= 2)
                {
                    odpoved.X = (sbyte)(2 - nova.x + 1 - 2);
                }
                if (nova.y > 2)
                {
                    odpoved.Y = (sbyte)(nova.x - 1 - 2);
                }
                else if (nova.y <= 2)
                {
                    odpoved.Y = (sbyte)(2 - nova.x + 1 - 2);
                }
            }
            PredchoziTahy = ZapisTah(PredchoziTahy, odpoved);
            odpoved.X = odpoved.X;
            return odpoved;
        }
        Smer krokOdZlodeje(char[,] castMapy, Zlodejik nova)
        {
            Smer odpoved = new Smer();
            if ((nova.x == 1 && ((nova.y == 1) || (nova.y == 2) || (nova.y == 3))) || (nova.x == 3 && ((nova.y == 1) || (nova.y == 2) || (nova.y == 3))) || (nova.x == 2 && ((nova.y == 1) || (nova.y == 3))))
            {
                if (nova.x > 2)
                {
                    odpoved.X = (sbyte)(nova.x - 2 - 2);
                }
                else if (nova.x == 2)
                {
                    odpoved.X = (sbyte)(nova.x - 1 - 2);
                }
                else
                {
                    odpoved.X = (sbyte)(nova.x + 2 - 2);
                }
                if (nova.y > 2)
                {
                    odpoved.Y = (sbyte)(nova.x - 2 - 2);
                }
                else if (nova.y == 2)
                {
                    odpoved.Y = (sbyte)(nova.x - 1 - 2);
                }
                else
                {
                    odpoved.Y = (sbyte)(nova.y + 2 - 2);
                }
            }
            else
            {
                if (nova.x > 2)
                {
                    odpoved.X = (sbyte)(nova.x - 3);
                }
                else if (nova.x == 2)
                {
                    odpoved.X = (sbyte)(nova.x - 2);
                }
                else
                {
                    odpoved.X = (sbyte)(nova.x + 3);
                }
                if (nova.y > 2)
                {
                    odpoved.Y = (sbyte)(nova.x - 3);
                }
                else if (nova.y == 2)
                {
                    odpoved.Y = (sbyte)(nova.x - 1);
                }
                else
                {
                    odpoved.Y = (sbyte)(nova.y + 3);
                }
            }
            PredchoziTahy = ZapisTah(PredchoziTahy, odpoved);
            return odpoved;
        }
        public Smer JdemPoNabidce(char[,] castMapy, Nabidky[] seznamNabidek, int volno, Zlodejik[] seznamZlodeju, int zaplneniZ, int penize, Souradnice souradnice)
        {
            Smer odpoved = new Smer();
            if (zaplneniZ == 1)
            {
                odpoved = krokOdZlodeje(castMapy, seznamZlodeju[zaplneniZ - 1]);
                return odpoved;
            }
            if (volno == 1)
            {
                if (seznamNabidek[volno - 1].cena < penize)
                {
                    odpoved = krokKNAbidce(castMapy, seznamNabidek[volno - 1]);
                }
                else
                {
                    return (JdemNekam(castMapy, seznamZlodeju, zaplneniZ, souradnice));
                }

            }
            else
            {
                int nejmensi = 10;
                int kolikata = 0;
                for (int i = 0; i < volno; i++)
                {
                    if (nejmensi > seznamNabidek[i].cena)
                    {
                        nejmensi = seznamNabidek[i].cena;
                        kolikata = i;
                    }
                }
                odpoved = krokKNAbidce(castMapy, seznamNabidek[kolikata]);
            }
            PredchoziTahy = ZapisTah(PredchoziTahy, odpoved);
            return odpoved;
        }
        public Smer JdemNekam(char[,] castMapy, Zlodejik[] seznamZlodeju, int zaplneniZ, Souradnice souradnice)
        {
            Smer odpoved = new Smer();
            int povoleno = 0, x = 0, y = 0, chyba = 0;
            while (povoleno == 0)
            {
                if (zaplneniZ == 0)
                {
                    if (PredchoziTahy[2].X == 0 && PredchoziTahy[2].Y == 0)
                    {
                        if (souradnice.X < 40) { odpoved.X = 1; x = 1; }
                        else { odpoved.X = -1; x = -1; }
                        if (souradnice.Y < 10) { odpoved.Y = 1; y = 1; }
                        else { odpoved.Y = -1; y = -1; }
                    }
                    else
                    {
                        if (chyba == 0)
                        {
                            odpoved = PredchoziTahy[2];
                        }
                        else
                        {
                            for (int i = 1; i < 4; i++)
                            {
                                for (int j = 1; j < 4; j++)
                                {
                                    if (castMapy[i, j] == ' ')
                                    {
                                        x = i - 2;
                                        y = j - 2;
                                        i = 5;
                                        j = 5;
                                        odpoved.X = (sbyte)x;
                                        odpoved.Y = (sbyte)y;
                                        chyba = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    odpoved = krokOdZlodeje(castMapy, seznamZlodeju[zaplneniZ - 1]);
                }
                if (castMapy[3 + x, 3 + y] != ' ') { chyba = 1; }
                else { povoleno = 1; }
                PredchoziTahy = ZapisTah(PredchoziTahy, odpoved);
            }
            return odpoved;
        }
        //dela krok nakupujiciho
        //deli se podle toho zda jsou nebo nejsou nabidky
        //na konci pricte pocet kol
        public override Smer Krok(char[,] castMapy, int penize, Souradnice souradnice)
        {
            Smer odpoved;
            Zlodejik[] seznamZlodeju = new Zlodejik[25];
            Nabidky[] seznamNabidek = new Nabidky[25];
            int zaplneniZ = 0;
            int volno = 0;
            if ((Nabidka(castMapy, seznamNabidek, volno, seznamZlodeju, zaplneniZ)) == true)
            {
                odpoved = JdemPoNabidce(castMapy, seznamNabidek, volno, seznamZlodeju, zaplneniZ, penize, souradnice);
            }
            else
            {
                odpoved = JdemNekam(castMapy, seznamZlodeju, zaplneniZ, souradnice);
            }
            pocet_kol++;
            PredchoziTahy = ZapisTah(PredchoziTahy, odpoved);
            return odpoved;
        }
        public Smer[] ZapisTah(Smer[] PT, Smer odpoved)
        {
            for (int i = 0; i < 2; i++)
            {
                PT[i] = PT[i + 1];
            }
            PT[2] = odpoved;
            return PT;
        }
    }
    struct Zlodeji
    {
        public Zlodejik[] zlodeji;
        public int zaplneni;
        public Zlodeji(Zlodejik[] zlodeji, int zaplneni)
        {
            this.zlodeji = zlodeji;
            this.zaplneni = zaplneni;
        }

    }
    struct Zlodejik
    {
        public int x, y;
        public Zlodejik(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
    struct Nabidky
    {
        public int kolo, cena, x, y;

        public Nabidky(int kolo, int cena, int x, int y)
        {
            this.kolo = kolo;
            this.cena = cena;
            this.x = x;
            this.y = y;

        }
    }
}*/
